package com.example.daggertest.di

import android.content.Context
import com.example.daggertest.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal object AppModule {
    @Singleton
    @Provides
    @JvmStatic
    fun providecontext(application: App): Context = application.applicationContext
}